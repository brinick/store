package store

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"time"
)

/*
 The data store

 Data for a day are stored in a JSON lines file called:
   <root>/<app>/<day>.json
 where <day> is of the form YYYY-MM-DD
*/

const (
	DateFormat     = time.DateOnly
	DateFormatPatt = "*-*-*"
)

// New creates a new store for the app in the default MY_DATA base directory
func New[T any](appName string) *store[T] {
	return &store[T]{Root: filepath.Join(dataDir(appName))}
}

func dataDir(appName string) string {
	switch runtime.GOOS {
	case "darwin", "linux":
		data, found := os.LookupEnv("MY_DATA")
		if !found {
			panic("Required base data directory env var MY_DATA is not set")
		}
		return filepath.Join(data, appName)
	default:
		panic(fmt.Sprintf("No support for OS %s", runtime.GOOS))
	}
}

/*
		patt := filepath.Join(root, dateFormatPatt(DateFormat))
		matches, _ := filepath.Glob(patt)
		var days []*dayStore
		for _, match := range matches {
			st, err := newDayStore(match)
			if err != nil {
				return nil, err
			}
			days = append(days, st)
		}

		// Sort the day stores by age
		sort.Slice(
			days,
			func(i, j int) bool {
				iage, err := days[i].age()
				if err != nil {
					return false
				}
				jage, err := days[j].age()
				if err != nil {
					return false
				}
				return iage > jage
			},
		)
		return &store{root, dayStores(days)}, nil
	}
*/

func todayDate() string {
	return time.Now().Format(DateFormat)
}

//----------------------------------------------------------------------

type store[T any] struct {
	// Root is the base directory in which data is stored for a specific app, of the form:
	//   <root>/<app>
	Root string

	// days is a slice of dayStore instances, where each dayStore wraps the access to a
	// particular day in the directory <Root>/<day>/
	days *dayStores[T]
}

// Today returns the data associated with today, empty if inexistant
func (s *store[T]) Today() *dayStore[T] {
	return &dayStore[T]{path: s.dayFile(todayDate())}
}

func (s *store[T]) Day(date string) *dayStore[T] {
	return &dayStore[T]{path: s.dayFile(date)}
}

func (s *store[T]) dayFile(date string) string {
	return filepath.Join(s.Root, fmt.Sprintf("%s.json", date))
}

// All returns all dayData
func (s *store[T]) All() *dayStores[T] {
	patt := filepath.Join(s.Root, fmt.Sprintf("%s.json", DateFormatPatt))
	dayFiles, _ := filepath.Glob(patt)
	var days dayStores[T]
	for _, df := range dayFiles {
		days = append(days, &dayStore[T]{path: df})
	}
	return &days
}

// Read will read the on-disk JSON files from <from> day to <to> day.
func (s *store[T]) Days(from, to *time.Time) (*dayStores[T], error) {
	// This returns a time-sorted (ascending order) slice of day stores
	stores, err := s.days.inDateRange(from, to)
	if err != nil {
		return nil, err
	}
	return stores, nil
}

//----------------------------------------------------------------------
//----------------------------------------------------------------------
//----------------------------------------------------------------------

/*
func toMidnight(t *time.Time) *time.Time {
	d := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.Local)
	return &d
}

*/
