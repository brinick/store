package store

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"time"
)

type dayStores[T any] []*dayStore[T]

func (d *dayStores[T]) inDateRange(from, to *time.Time) (*dayStores[T], error) {
	var ok = &dayStores[T]{}
	for _, ds := range *d {
		if _, err := ds.inDateRange(from, to); err != nil {
			return nil, err
		}

		*ok = append(*ok, ds)
	}

	return ok, nil
}

/*
func (d *dayStores) match(day string) *dayStore {
	for _, ds := range *d {
		if ds.name() == day {
			return ds
		}
	}

	return nil
}

func (d *dayStores) last(ndays int) (*dayStores, error) {
	var dds dayStores
	for _, ds := range *d {
		age, err := ds.age()
		if err != nil {
			return nil, err
		}
		if age <= ndays {
			dds = append(dds, ds)
		}
	}

	return &dds, nil
}

*/
//----------------------------------------------------------------------

type dayStore[T any] struct {
	// path is the full path to this day store JSON lines file
	path string
}

func (ds *dayStore[T]) Put(data T) error {
	content, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to marshal provided data: %v", err)
	}

	fd, err := os.OpenFile(ds.path, os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		return fmt.Errorf("failed to open %s for adding data: %v", ds.path, err)
	}
	defer fd.Close()

	fd.Write(content)
	fd.Write([]byte("\n"))
	return nil
}

func (ds *dayStore[T]) Get() ([]T, error) {
	content, err := os.ReadFile(ds.path)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, fmt.Errorf("cannot read day store file %s: %v", ds.path, err)
	}

	var items []T
	for _, line := range bytes.Split(content, []byte{'\n'}) {
		var t = new(T)
		if err := json.Unmarshal(line, t); err != nil {
			return nil, fmt.Errorf("failed to unmarshal '%s' (%v)", string(line), err)
		}
		items = append(items, *t)
	}
	return items, nil
}

func (ds *dayStore[T]) inDateRange(from, to *time.Time) (bool, error) {
	if (from == nil && to == nil) || from.Equal(*to) {
		return true, nil
	}

	date, err := ds.date()
	if err != nil {
		return false, err
	}

	if from == nil {
		return date.Before(*to), nil
	}

	if to == nil {
		return date.After(*from), nil
	}

	return from.Before(date) && date.Before(*to), nil
}

func (ds *dayStore[T]) date() (time.Time, error) {
	var value time.Time

	date, ok := strings.CutSuffix(filepath.Base(ds.path), filepath.Ext(ds.path))
	if !ok {
		return value, fmt.Errorf("Bad day store path format %s, does not have JSON file prefix", ds.path)
	}
	loc, _ := time.LoadLocation("Europe/Paris")
	value, err := time.ParseInLocation(DateFormat, date, loc)
	if err != nil {
		//This should not be possible, so we crash now if it occurs
		return value, fmt.Errorf("Found day store with unparseable date name %s: %v", date, err)
	}
	return value, nil
}

/*

// age gets the number of days ago with respect to today
func (ds *dayStore[T]) age() (int, error) {
	now := time.Now()
	today := toMidnight(&now)
	val, err := ds.date()
	if err != nil {
		return 0, err
	}
	hours := today.Sub(val).Hours()
	ago := int(hours) / 24
	return ago, nil
}


func (ds *dayStore) Add(entry dayStoreEntry) error {
	tgt, _ := os.OpenFile(ds.file(), os.O_CREATE|os.O_APPEND, 0644)
	return json.NewEncoder(tgt).Encode(entry)
}

func (ds *dayStore) overwrite(data dayEntry) error {
	content, err := json.Marshal(&data)
	if err != nil {
		return fmt.Errorf("failed to marshal day store JSON %s: %v", ds.file(), err)
	}

	return os.WriteFile(ds.file(), content, 0644)
}

func (ds *dayStore) update(data dayEntry) error {
	existing, err := ds.read()
	if err != nil {
		return fmt.Errorf("unable to read day store file %s: %v", ds.name(), err)
	}

	existing.Update(data)
	data = existing

}


func (ds *dayStore) Match() ([]string, error) {
	for _, datafile := range ds.data {
		data, err := os.ReadFile(datafile)
		if err != nil {
			return nil, err
		}
	}

}

func (ds *dayStore) _writeData() error {
	json.Marshal(ds.entries)
}

func (ds *dayStore) _addFile(data []byte, filetype string) error {
	name := fmt.Sprint(time.Now().UnixNano())
	return os.WriteFile(fmt.Sprintf("%d.%s", name, filetype), data, 0644)
}

// WriteJSON will add the new entry with the given data to the store as a JSON file
func (ds *dayStore) WriteJSON(data []byte) error {
	return ds._addFile(data, "json")
}

// Write will add the new entry with the given data to the store as a text file
func (ds *dayStore) Write(data []byte) error {
	return ds._addFile(data, "txt")
}

*/
